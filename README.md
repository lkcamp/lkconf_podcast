# LKConf Podcasts

This repo is used to store all media files for LKConf Podcasts (and if in the
future you create new cool podcasts, you can store here as well). It was created
to be used as a submodule of the website repo, so if you want to make some minor
changes in the page, you don't need to download hundreds of megabytes of
awesome content.

## Generating the feed

To add a new episode, you need to copy the files to the right place, modify
`feed.json` and run `ruby generate_rss.rb`. Don't know where is the right place,
and what to write at `feed.json`? Just follow the current directory structure,
check the following link and believe in your dreams:

- https://github.com/AnalyzePlatypus/PodcastFeedGenerator

## License

As LKConf videos, LKConf podcasts are licensed as CC-BY-SA.
