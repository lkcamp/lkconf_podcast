require "podcast_feed_generator"

json = JSON.parse(File.open("feed.json").read)

rss_feed = PodcastFeedGenerator::Generator.new.generate json

File.open("feed.xml", 'w') { |file| file.write(rss_feed) }
